class SpryShippingHelper
  class << self
    INFINITY = +1.0/0.0
    
    def box_count(object, quantity_string)
      category_prices = quantity_string.scan(CAN_FIT_PATTERN).collect{ |match| { :category => match[0], :quantity => match[1].to_i, :ratio => match[1].to_i == 0 ? INFINITY : Rational(1, match[1].to_i) } }      
      category_prices << {:category => "?", :quantity => 1, :ratio => Rational(1) }
      # pp category_prices
      line_item_groups = object.line_items.group_by{ |line_item| line_item.product.shipping_category && line_item.product.shipping_category.name || "?" }.update_values{ |v| v.sum(&:quantity) }
    
      # make sure we can handle all these items categories
      can_handle_categories = category_prices.collect{ |x| x[:category] }
      # pp can_handle_categories
      missing_categories = line_item_groups.collect{ |k,v| k } - can_handle_categories
      unless missing_categories.empty?
        # raise "This calculator cannot handle items with Shipping Category: #{missing_categories}"
        return 0.0
      end

      # debugger
      nil_categories = category_prices.any?{ |cat_price| cat_price[:ratio] == INFINITY && line_item_groups.has_key?(cat_price[:category]) }
      if nil_categories
        return INFINITY
      end
      # pp line_item_groups

      boxes = 0
      while line_item_groups.sum{ |k,v| v } > 0 do
        # fit the largest items in first
        space_remaining = Rational(1)
        boxes += 1
        # pp "opening a new box"
        # pp category_prices
        category_prices.sort{ |key, val| val[:ratio] }.reverse.each do |cat_price|
          # pp "considering category #{cat_price[:category]}..."
          next if !line_item_groups.has_key?(cat_price[:category]) or line_item_groups[cat_price[:category]] <= 0
          raise "cannot process category with #{cat_price[:ratio]} ratio" if cat_price[:ratio] == INFINITY
          if space_remaining >= cat_price[:ratio]
            cat_items_remaining = line_item_groups[cat_price[:category]]
          
            items_can_fit = (space_remaining / cat_price[:ratio])
            if items_can_fit > cat_items_remaining
              items_can_fit = cat_items_remaining
            end
            # items_can_fit = line_item_groups[cat_price[:category]] if items_can_fit > line_item_groups[cat_price[:category]]

            space_to_fill = items_can_fit * cat_price[:ratio]
            # pp "we can fit #{items_can_fit} #{cat_price[:category]} items into this box, which takes up #{space_to_fill} of the box."
            line_item_groups[cat_price[:category]] = line_item_groups[cat_price[:category]] - items_can_fit
            space_remaining -= space_to_fill
            # pp "just put #{items_can_fit} #{cat_price[:category]} items in a box.  There is #{space_remaining} space left in the box."
            # pp "items remaining: #{line_item_groups}"
          end
        end
      end
      boxes
    end
  
    private

    CAN_FIT_PATTERN = /([^:]+):([\d]+),?/
  end
end