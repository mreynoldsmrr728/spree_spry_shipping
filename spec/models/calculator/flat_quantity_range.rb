require 'spec_helper'
require 'spree/core/testing_support/factories'

describe Spree::Calculator::FlatQuantityRange do
  let(:calculator) { Spree::Calculator::FlatQuantityRange.new }
  let(:cat_large) { create(:shipping_category, :name => 'large') }
  let(:cat_small) { create(:shipping_category, :name => 'small') }
  let!(:product_large) { create(:product, :shipping_category => cat_large) }
  let!(:product_small) { create(:product, :shipping_category => cat_small) }
  let(:order) { mock_model Spree::Order, :line_items => [mock_model(Spree::LineItem, :amount => 10, :quantity => 4, :product => product_large), mock_model(Spree::LineItem, :amount => 20, :quantity => 6, :product => product_small)] }
  let(:order2) { mock_model Spree::Order, :line_items => [mock_model(Spree::LineItem, :amount => 10, :quantity => 1, :product => product_large)] }

  context "compute" do
    it "should have the same value for a min quantity of items and a max quantity of items" do
      1.should == 1
      calculator.stub :preferred_can_fit => "#{cat_large.name}:3,#{cat_small.name}:12"
      calculator.preferred_amount.should == 5.0
      calculator.compute(order).should == 10.0
    end
    
    it "should raise an error if it encounters a shipping category it can't handle" do
      calculator.stub :preferred_can_fit => "what:12"
      expect{ calculator.compute(order) }.to raise_error
    end
    
    it "should fit exactly" do
      calculator.stub :preferred_can_fit => "#{cat_large.name}:1"
      calculator.compute(order2).should == 5.0
    end
    
    # it "should compute amount correctly when first_item has a value" do
    #      calculator.stub :preferred_first_item => 1.0
    #      calculator.compute(order).round(2).should == 1.0
    #    end
    # 
    #    it "should compute amount correctly when additional_items has a value" do
    #      calculator.stub :preferred_additional_item => 1.0
    #      calculator.compute(order).round(2).should == 9.0
    #    end
    # 
    #    it "should compute amount correctly when additional_items and first_item have values" do
    #      calculator.stub :preferred_first_item => 5.0, :preferred_additional_item => 1.0
    #      calculator.compute(order).round(2).should == 14.0
    #    end
    # 
    #    it "should compute amount correctly when additional_items and first_item have values AND max items has value" do
    #      calculator.stub :preferred_first_item => 5.0, :preferred_additional_item => 1.0, :preferred_max_items => 3
    #      calculator.compute(order).round(2).should == 26.0
    #    end
    #    
    #    it "should allow creation of new object with all the attributes" do
    #      Spree::Calculator::FlexiRate.new(:preferred_first_item => 1, :preferred_additional_item => 1, :preferred_max_items => 1)
    #    end
  end
end
