module Spree
  class Calculator::FlatQuantityRange < Calculator
    preference :allow_multiple_boxes, :boolean, :default => false
    preference :amount, :decimal, :default => 5.0
    preference :can_fit, :string
    preference :order_more_than, :string
    attr_accessible :preferred_amount, :preferred_can_fit, :preferred_allow_multiple_boxes, :preferred_order_more_than
    def self.description
      I18n.t(:flat_quantity_range)
    end
    
    def available?(order)
      # debugger
      [ preferred_allow_multiple_boxes || SpryShippingHelper.box_count(order, preferred_can_fit) == 1,
        preferred_order_more_than.empty? || SpryShippingHelper.box_count(order, preferred_order_more_than) > 1
      ].all?
    end

    def compute(object)
      # debugger
      boxes = SpryShippingHelper.box_count(object, preferred_can_fit)
      sum = boxes * preferred_amount
    end
  end
end